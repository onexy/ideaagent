package shawnho.ideaagent;

import java.io.File;
import java.io.PrintStream;
import java.lang.instrument.Instrumentation;

/**
 * @author shawnho
 */
public class Application {
    public static void premain(String agentOps, Instrumentation inst) {
        try {
            String home = System.getProperty("user.home");
            File file = new File(home, "ideaagent.log");
            if (!file.exists()) file.createNewFile();
            PrintStream printStream = new PrintStream(file);
            System.setOut(printStream);
            System.setErr(printStream);
            inst.addTransformer(new IdeaTransformer());
            new AuthServer().run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
